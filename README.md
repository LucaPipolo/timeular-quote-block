# Timeular Quote Block

This repository contains a simple WordPress Gutenberg Block to show and share quotes.

<img src="./.preview-images/default.png" width="400">
<img src="./.preview-images/hover.png" width="400">

## :package: How to install

The plugin can be installed as a Git submodule:

1. Open your WordPress installation root path in the terminal.
2. `git submodule add --name quote-block -- git@gitlab.com:LucaPipolo/timeular-quote-block.git wp-content/plugins/quote-block`
3. Activate the plugin: `wp plugin activate quote-block`

…or using [Composer](https://getcomposer.org/).

## :hammer_and_wrench: How to setup locally

1. `git clone git@gitlab.com:LucaPipolo/timeular-quote-block.git`
2. `cd timeular-quote-block`
3. `npm install`
4. `npm run build`

## :gear: Available scripts

- `npm run build` · Builds the app for production to the `dist` folder.
- `npm run start` · Runs the app in the development mode.
- `npm run format:js` · Runs [Prettier](https://prettier.io/) to automatically format JavaScript code.
- `npm run lint:css` · Runs [Stylelint](https://stylelint.io/).
- `npm run lint:js` · Runs [ESLint](https://eslint.org/).
- `npm run packages-update` · Updates npm packages.

## :mag: What I did and why

- [`@wordpress/create-block`](https://www.npmjs.com/package/@wordpress/create-block) was used to scaffold a basic Gutenberg block structure. [Webpack](https://webpack.js.org/) is taking care of compiling the assets.
- PHP code is using namespaces and a PSR-4 class autoloader. Code standards are applied according to the [included PHPCS Ruleset file](https://gitlab.com/LucaPipolo/timeular-quote-block/-/blob/master/phpcs.ruleset.xml).
- To ensure a good code quality I setup PHPCS, ESLint and Stylelint. Also, the code is automatically formatted on `git commit` using [Prettier](https://prettier.io/), [Husky](https://www.npmjs.com/package/husky) and [lint-staged](https://www.npmjs.com/package/lint-staged).
- The quote and the cite fields are made with [WordPress RichText](https://developer.wordpress.org/block-editor/developers/richtext/) component. This provides options to format block content to make it bold, italics, linked, or use other formatting.
- I added [WordPress ToggleControl](https://developer.wordpress.org/block-editor/components/toggle-control/) component to the block sidebar. This allows editors to decide which Social Networks links show in the frontend.
- I used `wp i18n make-pot ./ languages/timeular-quote-block.pot` [CLI command](https://developer.wordpress.org/cli/commands/i18n/) to make the block ready to be translated. See [WordPress internationalization docs](https://developer.wordpress.org/block-editor/developers/internationalization/) for more information.

## :notebook: Notes

- Without knowing more context about the usage of the block, I decided to use a simple block instead of a dynamic one. I think we are not matching any of the two primary use cases described by the [WordPress documentation](https://developer.wordpress.org/block-editor/tutorials/block-tutorial/creating-dynamic-blocks/).
- The LinkedIn share button only shares the URL instead of prepopulating a post message with the quote and the cite. This because [LinkedIn requires their API package to be used](https://docs.microsoft.com/en-us/linkedin/consumer/integrations/self-serve/share-on-linkedin?context=linkedin/consumer/context) to ensure harmful messages are not posted. Of course, the API could be set and the quote posted as a message, but I preferred to not invest time on it as I thought was not the main goal of the task.

## :rocket: Further possible improvements

- A [WordPress Text](https://developer.wordpress.org/block-editor/components/text/) component could be added to the block sidebar panel to manage a list of hashtags to add on the shared posts.
- A Continuos Deployment tool could be used to automatically generate the `dist` folder on deployment.
- If the block becomes more complex, icons could be better managed. Them can be extracted to an isolated React component and Storybook can be introduced to preview them. At the current status, this would have been overkilling.

## :page_facing_up: License

[GPL-2.0 License](https://gitlab.com/LucaPipolo/timeular-quote-block/-/blob/master/LICENSE) © [Luca Pipolo](mailto:dev@lucapipolo.com)
