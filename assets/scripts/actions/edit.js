import { getBlockDefaultClassName } from '@wordpress/blocks';
import { RichText } from '@wordpress/block-editor';
import { __ } from '@wordpress/i18n';

/**
 * Internal dependencies.
 */
import sidebarControls from '../helpers/sidebarControls.js';

/**
 * Styles.
 */
import '../../styles/editor.scss';

/**
 * Gets block default class name to create BEM prefixed classes.
 */
const defaultClassName = getBlockDefaultClassName('timeular/quote-block');

/**
 * Describes the structure of the block in the context of the editor.
 * This represents what the editor will render when the block is used.
 *
 * @param {Object} [props] - Properties passed from the editor.
 * @param {Object} [props.className] - Class name generated for the block.
 * @param {Object} [props.attributes] - Attributes passed from the editor.
 * @param {Object} [props.setAttributes] - Function to change attributes value.
 *
 * @return {WPElement} Element to render.
 */
export default function edit({ className, attributes, setAttributes }) {
  return (
    <blockquote className={className}>
      {sidebarControls(attributes, setAttributes)}
      <RichText
        tagName="p"
        className={`${defaultClassName}__quote`}
        onChange={(quote) => setAttributes({ quote })}
        value={attributes.quote}
        placeholder={__('Write quote…', 'timeular-quote-block')}
      />
      <div className={`${defaultClassName}__footer`}>
        <RichText
          tagName="cite"
          className={`${defaultClassName}__cite`}
          onChange={(cite) => setAttributes({ cite })}
          value={attributes.cite}
          placeholder={__('Write cite…', 'timeular-quote-block')}
        />
      </div>
    </blockquote>
  );
}
