import { getBlockDefaultClassName } from '@wordpress/blocks';
import { RichText } from '@wordpress/block-editor';
import { Icon } from '@wordpress/components';

/**
 * Gets block default class name to create BEM prefixed classes.
 */
const defaultClassName = getBlockDefaultClassName('timeular/quote-block');

/**
 * Import icons.
 */
import icons from '../helpers/icons.js';

/**
 * Defines the way in which the different attributes should be combined into
 * the final markup, which is then serialized by the block editor into
 * `post_content`.
 *
 * @param {Object} [props] Properties passed from the editor.
 * @param {Object} [props.attributes] Attributes passed from the editor.
 *
 * @return {WPElement} Element to render.
 */
export default function save({ attributes }) {
  const { quote, cite } = attributes;

  const hostname = window.location.hostname;
  const textToShare = `${quote} ${cite ? ' - ' + cite : ''}`;
  const escapedTextToShare = textToShare.replace(/<\/?[^>]+(>|$)/g, '');

  return (
    <blockquote>
      <RichText.Content
        className={`${defaultClassName}__quote`}
        tagName="p"
        value={quote}
      />
      {(cite ||
        attributes.facebook ||
        attributes.twitter ||
        attributes.linkedin) && (
        <div className={`${defaultClassName}__footer`}>
          {cite && (
            <RichText.Content
              className={`${defaultClassName}__cite`}
              tagName="cite"
              value={cite}
            />
          )}
          {(attributes.facebook ||
            attributes.twitter ||
            attributes.linkedin) && (
            <div className={`${defaultClassName}__share`}>
              {attributes.facebook && (
                <a
                  className="share__link"
                  href={`https://www.facebook.com/sharer/sharer.php?u=${hostname}&quote=${escapedTextToShare}`}
                >
                  <Icon className="icon" icon={icons.facebook} />
                </a>
              )}
              {attributes.twitter && (
                <a
                  className="share__link"
                  href={`https://twitter.com/intent/tweet?url=${hostname}&text=${escapedTextToShare}`}
                >
                  <Icon className="icon" icon={icons.twitter} />
                </a>
              )}
              {attributes.linkedin && (
                <a
                  className="share__link"
                  href={`https://www.linkedin.com/sharing/share-offsite/?url=${hostname}`}
                >
                  <Icon className="icon" icon={icons.linkedin} />
                </a>
              )}
            </div>
          )}
        </div>
      )}
    </blockquote>
  );
}
