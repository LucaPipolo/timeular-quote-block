import { InspectorControls } from '@wordpress/block-editor';
import { ToggleControl, PanelBody, PanelRow } from '@wordpress/components';
import { __ } from '@wordpress/i18n';

/**
 * Adds block sidebar panel controls.
 *
 * @param {Object} attributes - Attributes passed from the editor.
 * @param {Object} setAttributes - Function to change attributes value.
 */
export default function sidebarControls(attributes, setAttributes) {
  return (
    <InspectorControls>
      <PanelBody
        title={__('Social Network Share Options', 'timeular-quote-block')}
        initialOpen={true}
      >
        <PanelRow>
          <ToggleControl
            label="Facebook"
            checked={attributes.facebook}
            onChange={(facebook) => setAttributes({ facebook })}
          />
        </PanelRow>
        <PanelRow>
          <ToggleControl
            label="Twitter"
            checked={attributes.twitter}
            onChange={(twitter) => setAttributes({ twitter })}
          />
        </PanelRow>
        <PanelRow>
          <ToggleControl
            label="LinkedIn"
            checked={attributes.linkedin}
            onChange={(linkedin) => setAttributes({ linkedin })}
          />
        </PanelRow>
      </PanelBody>
    </InspectorControls>
  );
}
