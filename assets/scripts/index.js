import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';

/**
 * Internal dependencies.
 */
import edit from './actions/edit';
import save from './actions/save';

/**
 * Styles.
 */
import '../styles/style.scss';

/**
 * Registers block type definition.
 */
registerBlockType('timeular/quote-block', {
  title: __('Timeular Quote', 'timeular-quote-block'),
  description: __(
    'A simple WordPress Gutenberg Block to show and share quotes.',
    'timeular-quote-block'
  ),
  category: 'common',
  icon: 'editor-quote',
  supports: {
    html: false,
  },
  example: {
    attributes: {
      author: 'Luca Pipolo',
      pages: 500,
    },
  },
  attributes: {
    quote: {
      type: 'string',
      source: 'html',
      selector: 'p',
    },
    cite: {
      type: 'string',
      source: 'html',
      selector: 'cite',
    },
    facebook: {
      type: 'boolean',
      default: true,
    },
    twitter: {
      type: 'boolean',
      default: true,
    },
    linkedin: {
      type: 'boolean',
      default: true,
    },
  },
  edit,
  save,
});
