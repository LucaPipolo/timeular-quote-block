��          D      l       �   <   �      �      �      �   q     E   r  +   �     �     �                          A simple WordPress Gutenberg Block to show and share quotes. Social Network Share Options Write cite… Write quote… Project-Id-Version: timeular-quote-block
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/timeular-quote-block
PO-Revision-Date: 2020-08-02 13:14+0200
Last-Translator: Lu
Language-Team: Luca Pipolo
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4
X-Domain: timeular-quote-block
 Un semplice blocco di Gutenberg per mostrare e condividere citazioni. Opzioni di condivisione sui Social Networks Aggiungi un autore… Scrivi una citazione… 