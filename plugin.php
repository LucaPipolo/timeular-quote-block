<?php // phpcs:ignore

// phpcs:disable
/**
 * Plugin Name: timeular-quote-block
 * Plugin URI: https://gitlab.com/LucaPipolo/timeular-quote-block
 * Description: A simple WordPress Gutenberg Block to show and share quotes.
 * Author: Luca Pipolo
 * Author URI: https://www.lucapipolo.com
 * Version: 1.0.0
 * License: GPL-2.0-or-later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */
// phpcs:enable

namespace Timeular\QuoteBlock;

if (!defined('ABSPATH')) {
  header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
  exit;
}

/**
 * Loads PSR-4-style plugin classes.
 */
function classloader($class) {
  static $ns_offset;
  if (strpos($class, __NAMESPACE__ . '\\') === 0) {
    if ($ns_offset === NULL) {
      $ns_offset = strlen(__NAMESPACE__) + 1;
    }
    include __DIR__ . '/src/' . strtr(substr($class, $ns_offset), '\\', '/') . '.php';
  }
}

spl_autoload_register(__NAMESPACE__ . '\classloader');

add_action('init', __NAMESPACE__ . '\Plugin::init');
