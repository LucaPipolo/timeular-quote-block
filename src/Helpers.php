<?php

namespace Timeular\QuoteBlock;

/**
 * Helper methods.
 */
class Helpers {

  /**
   * Plugin base URL.
   *
   * @var string
   */
  private static $baseUrl;

  /**
   * The absolute filesystem base path of this plugin.
   *
   * @return string
   *   The plugin absolute filesystem base path.
   */
  public static function getBasePath(): string {
    return dirname(__DIR__);
  }

  /**
   * The base URL path to this plugin's folder.
   *
   * Uses plugins_url() instead of plugin_dir_url() to avoid a trailing slash.
   *
   * @return string
   *   The plugin base URL path.
   */
  public static function getBaseUrl(): string {
    if (!isset(static::$baseUrl)) {
      static::$baseUrl = plugins_url('', static::getBasePath() . '/plugin.php');
    }

    return static::$baseUrl;
  }

}
