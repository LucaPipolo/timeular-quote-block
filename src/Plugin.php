<?php

namespace Timeular\QuoteBlock;

/**
 * Main plugin functionality.
 */
class Plugin {

  /**
   * Prefix for naming.
   *
   * @var string
   */
  const PREFIX = 'timeular-quote-block';

  /**
   * Gettext localization domain.
   *
   * @var string
   */
  const L10N = self::PREFIX;

  /**
   * Plugin initialization method.
   *
   * @implements init
   */
  public static function init() {
    static::enqueueAssets();
    static::registerBlock();
  }

  /**
   * Enqueues Gutenberg block assets.
   */
  public static function enqueueAssets() {
    $basePath = Helpers::getBasePath();

    $scriptAssetPath = "$basePath/dist/index.asset.php";
    if (!file_exists($scriptAssetPath)) {
      throw new \Error(
        'You need to run `npm start` or `npm run build` for the "timeular/quote-block" block first.'
      );
    }

    $script = require $scriptAssetPath;
    wp_register_script(
      'timeular/quote-block-editor-script',
      Helpers::getBaseURL() . '/dist/index.js',
      $script['dependencies'],
      $script['version']
    );

    $editorStyle = '/dist/index.css';
    wp_register_style(
      'timeular/quote-block-editor-style',
      Helpers::getBaseURL() . $editorStyle,
      [],
      filemtime("$basePath/$editorStyle")
    );

    $style = '/dist/style-index.css';
    wp_register_style(
      'timeular/quote-block-style',
      Helpers::getBaseURL() . $style,
      [],
      filemtime("$basePath/$style")
    );

    // Sets translated strings.
    wp_set_script_translations('timeular/quote-block-editor-script', static::L10N, Helpers::getBasePath() . '/languages');
  }

  /**
   * Registers Gutenberg block.
   */
  public static function registerBlock() {
    register_block_type('timeular/quote-block', [
      'editor_script' => 'timeular/quote-block-editor-script',
      'editor_style' => 'timeular/quote-block-editor-style',
      'style' => 'timeular/quote-block-style',
    ]);
  }

}
